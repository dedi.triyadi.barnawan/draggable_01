import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Offset pos = Offset(50, 50);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: pos.dy,
            left: pos.dx,
            child: Draggable(
              child: SizedBox(
                width: 100,
                height: 100,
                child: Material(
                  color: Colors.blue,
                  // shape: StadiumBorder(),
                  elevation: 3,
                ),
              ),
              childWhenDragging: Container(),
              feedback: SizedBox(
                width: 100,
                height: 100,
                child: Material(
                  color: Colors.blue.withOpacity(0.7),
                  // shape: StadiumBorder(),
                  elevation: 3,
                ),
              ),
              onDraggableCanceled: (view, offset) {
                setState(() {
                  pos = offset;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
